package com.wells_fargo.Util;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.util.PropertyUtil;

public class WellsForgoProperties {

	public static PropertyUtil propertyUtil = ConfigurationManager.getBundle();
	public static final String WELLS_FORGO_HOME_PAGE =
			propertyUtil.getString("wells.forgo.home.page", "Wells Forgo Home");
	public static final String COMMERCIAL_CUSTOMER_SERVICE_PAGE =
			propertyUtil.getString("commercial.customer.service.page", "Commercial customer service");
	public static final String COMMERCIAL_PAGE =
			propertyUtil.getString("commercial.page", "Commercial");
	public static final String 	BORROWINGANDCREDIT_PAGE =
			propertyUtil.getString("borrowingandcredit.page", "Borrowing and Credit");

	public static final String LINK = propertyUtil.getString("link", "link");
	public static final String FAIL = propertyUtil.getString("fail", "fail");
	public static final String CUSTOMER_SERVICE = propertyUtil.getString("customer.service", "Customer Service");
	public static final String COMMERCIAL = propertyUtil.getString("commercial", "Commercial");
	public static final String INTERNATIONAL_ROUTING_NUMBERS = propertyUtil.getString("international.routing.numbers", "International Routing Numbers");
	public static final String CITY = propertyUtil.getString("city", "City");
	public static final String NAME  = propertyUtil.getString("name", "Name");
	public static final String CONTACT_FORM  = propertyUtil.getString("contact.form", "contact form");
	public static final String CONTACT_US_ONLINE  = propertyUtil.getString("contact.us.online", "Contact Us Online");

}
