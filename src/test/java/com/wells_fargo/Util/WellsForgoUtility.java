package com.wells_fargo.Util;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Point;
import org.openqa.selenium.interactions.Actions;

import com.qmetry.qaf.automation.ui.WebDriverTestBase;
import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebDriver;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class WellsForgoUtility {
	
		public void scrollToCenter(QAFWebElement ele) {
			QAFExtendedWebDriver driver = new WebDriverTestBase().getDriver();
			try {
				Point p = ele.getLocation();
				System.out.println("Element is at " + p.getX() + ":" + p.getY());
				long height = (long) ((JavascriptExecutor) driver)
						.executeScript("return document.documentElement.clientHeight");
				System.out.println("And View Height is : " + height);
				((JavascriptExecutor) driver)
						.executeScript("window.scroll(" + p.getX() + "," + (p.getY() - (height / 2)) + ");");

				// Added for Scroll to Center
				Actions actions = new Actions(driver);
				actions.moveToElement(ele);
				actions.perform();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		
}


