package com.wells_fargo.Runner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(features = "resources/Features", glue = {"com.wells_fargo.Steps"},
monochrome = true,
plugin={"pretty","html:target/HtmlReports"}


)
public class TestRunner {

}
