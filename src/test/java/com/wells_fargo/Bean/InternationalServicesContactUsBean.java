package com.wells_fargo.Bean;

import com.qmetry.qaf.automation.data.BaseDataBean;
import com.qmetry.qaf.automation.util.RandomStringGenerator.RandomizerTypes;
import com.qmetry.qaf.automation.util.Randomizer;

public class InternationalServicesContactUsBean extends BaseDataBean {
	
	@Randomizer(dataset={"Mr.", "Mrs.", "Miss.", "Ms.", "Dr."})
	private String name;
	@Randomizer(type = RandomizerTypes.LETTERS_ONLY, length = 6)
	private String firstName;
	@Randomizer(type = RandomizerTypes.LETTERS_ONLY, length = 6)
	private String surname;
	@Randomizer(type = RandomizerTypes.LETTERS_ONLY, length = 10)
	private String corporateTitle;
	@Randomizer(type = RandomizerTypes.LETTERS_ONLY, length = 6)
	private String companyName;
	@Randomizer(type = RandomizerTypes.MIXED, length = 15)
	private String companyWebsite;
	@Randomizer(dataset={"Less than $5 million", "$5 to $20 million", "$100 million and higher"})
	private String annualRevenue;
	@Randomizer(dataset={"Yes", "No"})
	private String isWellsCustomer;
	@Randomizer(type=RandomizerTypes.MIXED, length = 15)
	private String firstAddress;
	@Randomizer(type=RandomizerTypes.MIXED, length = 15)
	private String secondAddress;
	@Randomizer(type = RandomizerTypes.LETTERS_ONLY, length = 6)
	private String city;
	@Randomizer(type = RandomizerTypes.LETTERS_ONLY, length = 8)
	private String state;
	@Randomizer(type = RandomizerTypes.DIGITS_ONLY, length=6)
	private String zip;
	@Randomizer(type = RandomizerTypes.LETTERS_ONLY, length = 6)
	private String country;
	@Randomizer(type=RandomizerTypes.DIGITS_ONLY, length=10)
	private String usPhoneNumber;
	@Randomizer(type=RandomizerTypes.DIGITS_ONLY, length=10)
	private String internationalPhoneNumber;
	@Randomizer(suffix="@gmail.com", length = 6)
	private String email;
	@Randomizer(dataset={"Foreign Exchange", "Banking Services for Financial Institutions", "International Treasury Management"})
	private String inforamtionNeeded;
	@Randomizer(type=RandomizerTypes.LETTERS_ONLY, length = 20)
	private String help;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getCorporateTitle() {
		return corporateTitle;
	}
	public void setCorporateTitle(String corporateTitle) {
		this.corporateTitle = corporateTitle;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getCompanyWebsite() {
		return companyWebsite;
	}
	public void setCompanyWebsite(String companyWebsite) {
		this.companyWebsite = companyWebsite;
	}
	public String getAnnualRevenue() {
		return annualRevenue;
	}
	public void setAnnualRevenue(String annualRevenue) {
		this.annualRevenue = annualRevenue;
	}
	public String getIsWellsCustomer() {
		return isWellsCustomer;
	}
	public void setIsWellsCustomer(String isWellsCustomer) {
		this.isWellsCustomer = isWellsCustomer;
	}
	public String getFirstAddress() {
		return firstAddress;
	}
	public void setFirstAddress(String firstAddress) {
		this.firstAddress = firstAddress;
	}
	public String getSecondAddress() {
		return secondAddress;
	}
	public void setSecondAddress(String secondAddress) {
		this.secondAddress = secondAddress;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getUsPhoneNumber() {
		return usPhoneNumber;
	}
	public void setUsPhoneNumber(String usPhoneNumber) {
		this.usPhoneNumber = usPhoneNumber;
	}
	public String getInternationalPhoneNumber() {
		return internationalPhoneNumber;
	}
	public void setInternationalPhoneNumber(String internationalPhoneNumber) {
		this.internationalPhoneNumber = internationalPhoneNumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getInforamtionNeeded() {
		return inforamtionNeeded;
	}
	public void setInforamtionNeeded(String inforamtionNeeded) {
		this.inforamtionNeeded = inforamtionNeeded;
	}
	public String getHelp() {
		return help;
	}
	public void setHelp(String help) {
		this.help = help;
	}
}
