package com.wells_fargo.Bean;

import com.qmetry.qaf.automation.data.BaseDataBean;

public class CommercialContactUsBean extends BaseDataBean {
	
	private String title;
	private String firstName;
	private String surname;
	private String corporateTitle;
	private String companyName;
	private String companyWebsite;
	private String annualRevenue;
	private String isWellsCustomer;
	private String firstAddress;
	private String secondAddress;
	private String city;
	private String state;
	private String zip;
	private String country;
	private String usPhoneNumber;
	private String internationalPhoneNumber;
	private String email;
	private String inforamtionNeeded;
	private String help;
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getCorporateTitle() {
		return corporateTitle;
	}
	public void setCorporateTitle(String corporateTitle) {
		this.corporateTitle = corporateTitle;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getCompanyWebsite() {
		return companyWebsite;
	}
	public void setCompanyWebsite(String companyWebsite) {
		this.companyWebsite = companyWebsite;
	}
	public String getAnnualRevenue() {
		return annualRevenue;
	}
	public void setAnnualRevenue(String annualRevenue) {
		this.annualRevenue = annualRevenue;
	}
	public String getIsWellsCustomer() {
		return isWellsCustomer;
	}
	public void setIsWellsCustomer(String isWellsCustomer) {
		this.isWellsCustomer = isWellsCustomer;
	}
	public String getFirstAddress() {
		return firstAddress;
	}
	public void setFirstAddress(String firstAddress) {
		this.firstAddress = firstAddress;
	}
	public String getSecondAddress() {
		return secondAddress;
	}
	public void setSecondAddress(String secondAddress) {
		this.secondAddress = secondAddress;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getUsPhoneNumber() {
		return usPhoneNumber;
	}
	public void setUsPhoneNumber(String usPhoneNumber) {
		this.usPhoneNumber = usPhoneNumber;
	}
	public String getInternationalPhoneNumber() {
		return internationalPhoneNumber;
	}
	public void setInternationalPhoneNumber(String internationalPhoneNumber) {
		this.internationalPhoneNumber = internationalPhoneNumber;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getInforamtionNeeded() {
		return inforamtionNeeded;
	}
	public void setInforamtionNeeded(String inforamtionNeeded) {
		this.inforamtionNeeded = inforamtionNeeded;
	}
	public String getHelp() {
		return help;
	}
	public void setHelp(String help) {
		this.help = help;
	}
}
