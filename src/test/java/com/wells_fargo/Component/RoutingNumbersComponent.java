package com.wells_fargo.Component;

import java.util.List;
import org.hamcrest.Matchers;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebComponent;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.qmetry.qaf.automation.util.Validator;
import com.wells_fargo.Pages.RoutingNumbersPage;
import com.wells_fargo.Util.WellsForgoProperties;

public class RoutingNumbersComponent extends QAFWebComponent {
	@FindBy(locator = "list.routing.numbers.component.columns.loc")
	private List<QAFWebElement> coloumnsList;
	@FindBy(locator = "list.routing.numbers.component.rows.loc")
	private List<RowsComponent> rowsComponent;

	public RoutingNumbersComponent(String locator) {
		super(locator);
	}

	// Rows Component
	public class RowsComponent extends QAFWebComponent {

		@FindBy(locator = "routing.numbers.rows.component.values.list.loc")
		private List<QAFWebElement> valuesList;

		public RowsComponent(String locator) {
			super(locator);
		}

		public List<QAFWebElement> getValuesList() {
			return valuesList;
		}
	}

	public List<QAFWebElement> getColoumnsList() {
		return coloumnsList;
	}

	public List<RowsComponent> getRowsComponent() {
		return rowsComponent;
	}

	// Verifies data of city and its respective name
	public void verifyData(String city, String name) {
		RoutingNumbersPage routingNumbersPage = new RoutingNumbersPage();
		int indexOfCity = 0, indexOfName = 0;
		List<RowsComponent> actualRows = getRowsComponent();
		int count = 0;
		Boolean CityFlag=false,nameFlag=false;;
		
		if (routingNumbersPage.getUsRoutingNumbersTable().isPresent() 
				&& routingNumbersPage.getUsRoutingNumbersTable().isDisplayed()){
		for (QAFWebElement coloumn : coloumnsList) {
			if (coloumn.getText().trim()
					.equals(WellsForgoProperties.CITY) && CityFlag==false) {
				indexOfCity = count;
				CityFlag=true;
			} else if (coloumn.getText().trim()
					.equals(WellsForgoProperties.NAME) && nameFlag==false) {
				indexOfName = count;
				nameFlag=true;
			}
			count++;
		}
		}
		
		for (RowsComponent row : actualRows) {
			 if(row.getValuesList().get(indexOfCity).getText().trim().equals(city))
				 {
				 if(row.getValuesList().get(indexOfName).getText().trim().isEmpty()){
					 Reporter.logWithScreenShot("Bank name field is empty of" + row.getValuesList().get(indexOfCity) + "city");
			     }
				 else{
					 Validator.verifyThat(row.getValuesList().get(indexOfName).getText().trim(),Matchers.equalTo(name));
				 }
			}
		}
	}
}

