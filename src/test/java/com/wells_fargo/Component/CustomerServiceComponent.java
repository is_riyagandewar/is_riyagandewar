package com.wells_fargo.Component;

import java.util.List;

import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebComponent;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;

public class CustomerServiceComponent extends QAFWebComponent {

	@FindBy(locator = "commercial.customer.service.routing.numbers.list.loc")
	private List<QAFWebElement> routingNumbersList;

	public CustomerServiceComponent(String locator) {
		super(locator);
	}

	public List<QAFWebElement> getRoutingNumbersList() {
		return routingNumbersList;
	}
	
	
}
