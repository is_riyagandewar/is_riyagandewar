package com.wells_fargo.Component;

import java.util.List;
import com.qmetry.qaf.automation.core.MessageTypes;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebComponent;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.wells_fargo.Util.WellsForgoProperties;

public class HomePageComponent extends QAFWebComponent{

	@FindBy(locator = "home.page.menu.list.loc")
	private List<QAFWebElement> menuList;
	
	public HomePageComponent(String locator) {
		super(locator);
	}
	
	public List<QAFWebElement> getMenuList() {
		return menuList;
	}
	
	public void clickOnElement(String elementName) {
				List<QAFWebElement> homeMenuList = getMenuList();
				for(QAFWebElement menus:homeMenuList ){
					menus.assertVisible();
					if(menus.getText().trim().equalsIgnoreCase(elementName))
					{
						menus.click();
						Reporter.log(WellsForgoProperties.COMMERCIAL + " " + WellsForgoProperties.LINK
								+ " clicked successfully", MessageTypes.Pass);
					}
				}
	}
}
