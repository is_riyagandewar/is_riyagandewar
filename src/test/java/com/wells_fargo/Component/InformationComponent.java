package com.wells_fargo.Component;


import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebComponent;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
//Component for Information on CEO Sign In Page
public class InformationComponent extends QAFWebComponent {

	@FindBy(locator="information.header")
	public QAFWebElement headerText;
	@FindBy(locator="information.content")
	public QAFWebElement contentText;

	public InformationComponent(String locator) {
		super(locator);
		
	}
	
	public QAFWebElement getHeaderText() {
		return headerText;
	}
	
	public QAFWebElement getContentText() {
		return contentText;
	}

}
