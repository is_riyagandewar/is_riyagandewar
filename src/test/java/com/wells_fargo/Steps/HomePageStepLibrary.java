package com.wells_fargo.Steps;

import com.wells_fargo.Pages.HomeTestPage;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class HomePageStepLibrary {

	@Given("user navigates to wellsfargo home page")
	public void userNavigatesToWellsfargoHomePage() {
		HomeTestPage homeTestPage = new HomeTestPage();
		homeTestPage.launchPage(null);
		homeTestPage.waitForPageToLoad();
	}
	
	@Then("user verify wellsfargo home page is opened")
	public void verifyWellsfargoHomePage() {
		HomeTestPage homeTestPage = new HomeTestPage();
		homeTestPage.verifyWellsForgoHomePage();

	}
	
	@When("navigates to {string} page")
	public void navigatesToPage(String menuLink) {
		HomeTestPage homeTestPage = new HomeTestPage();
		homeTestPage.getMenuComponent().clickOnElement(menuLink);
	}
	
}
