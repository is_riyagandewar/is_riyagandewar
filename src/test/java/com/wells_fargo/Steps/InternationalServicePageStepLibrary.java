package com.wells_fargo.Steps;

import com.wells_fargo.Pages.CommercialPage;
import com.wells_fargo.Pages.InternationalContactformPage;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;

public class InternationalServicePageStepLibrary {

	
	@And("clicks on {string} link in Routing Number page")
	public void clicksOnLinkInRoutingNumberPage(String str0) {
		CommercialPage commercialPage = new CommercialPage();
		commercialPage.clickOnElement(str0);
	}
	
	@Then("customer fill contact form on International Service page")
	public void customerFillContactFormOnInternationalServicepage() {
		InternationalContactformPage internationalContactformPage = new InternationalContactformPage();
		internationalContactformPage.fillData();
	}
}
