package com.wells_fargo.Steps;

import com.wells_fargo.Pages.CEOSignInPage;
import com.wells_fargo.Pages.CommercialContactUsPage;
import com.wells_fargo.Pages.CommercialFinancingServicesPage;
import com.wells_fargo.Pages.HomeTestPage;
import com.wells_fargo.Pages.RoutingNumbersPage;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class ceoSignInPageStepLibrary {

	@Given("navigate to ceo sign in page")
	public void navigateToSignInPage() {
		CommercialFinancingServicesPage commercialPage = new CommercialFinancingServicesPage();
		commercialPage.getSignInButton().click();
	}

	@Then("verify information on sign in page")
	public void verifyInformation() {

		CEOSignInPage ceoSignInPage =new CEOSignInPage();
		ceoSignInPage.verifyInformation();
	
	}
	
	@When("fill incorrect <companyid> <userid> <password> signin data")
	@When("fill incorrect {string}, {string}, {string} signin data")
	public void fillSingnInData(String companyid, String userid, String password) {

	 CEOSignInPage ceoSignInPage = new CEOSignInPage();
	 ceoSignInPage.fillSignInData(companyid,userid,password);
		
	}
	
	@Then("verify error message")
	public void verifyErrorMessage() {

		CEOSignInPage ceoSignInPage = new CEOSignInPage();
		ceoSignInPage.verifyErrorMessage();
	}
	
	}
