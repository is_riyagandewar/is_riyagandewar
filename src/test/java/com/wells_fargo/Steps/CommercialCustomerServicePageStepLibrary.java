package com.wells_fargo.Steps;

import com.wells_fargo.Pages.CommercialContactUsPage;
import com.wells_fargo.Pages.CommercialCustomerServicePage;
import com.wells_fargo.Pages.CommercialPage;
import com.wells_fargo.Pages.HomeTestPage;
import com.wells_fargo.Pages.RoutingNumbersPage;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class CommercialCustomerServicePageStepLibrary {

	@When("click on commercial page")
	public void navigateToCommercialPage() {
		HomeTestPage homePage = new HomeTestPage();
		homePage.getCommercialLink().click();
	}
	
	@Given("clicks on {string} link in Commercial page")
	public void clicksOnLinkInCommercialPage(String str0) {
		CommercialPage commercialPage = new CommercialPage();
		commercialPage.clickOnElement(str0);
	}

	@Then("user verify customer service page is opened")
	public void verifyCustomerservicePage() {
		CommercialCustomerServicePage commercialCustomerServicePage = new CommercialCustomerServicePage();
		commercialCustomerServicePage.verifyCustomerServicePage();

	}
	
	@When("clicks on {string} link in Customer Service page")
	public void clicksOnLinkInCustomerServicePage(String str0) {
		CommercialPage commercialPage = new CommercialPage();
		commercialPage.clickOnElement(str0);
	}
	
	@And("user fill contact us form on Commercial page")
	public void userFillContactUsFormOnCommercialPage() {
		CommercialContactUsPage CommercialContactUsPage = new CommercialContactUsPage();
		CommercialContactUsPage.fillData();
	}
	
	@And("user verifies {string} and {string} of U.S. locations in Routing Numbers page")
	public void userStoresLocations(String city, String name) {
		RoutingNumbersPage routingNumbersPage = new RoutingNumbersPage();
		routingNumbersPage.getUsRoutingNumbersTable().verifyData(city, name);
	}

}
