package com.wells_fargo.Pages;

import java.util.List;

import com.qmetry.qaf.automation.core.ConfigurationManager;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.wells_fargo.Component.InformationComponent;

public class CEOSignInPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "company_id.input")
	private QAFWebElement companyId;
	@FindBy(locator = "user_id.input")
	private QAFWebElement userId;
	@FindBy(locator = "password.input")
	private QAFWebElement password;
	@FindBy(locator = "error_message.text")
	private QAFWebElement errorMessage;
	@FindBy(locator = "sign_in.button")
	private QAFWebElement signInButton;
	@FindBy(locator = "information.component")
	List<InformationComponent> informationComponent;

	public List<InformationComponent> getInformationComponent() {
		return informationComponent;
	}

	public QAFWebElement getCompanyId() {
		return companyId;
	}

	public QAFWebElement getUserId() {
		return userId;
	}

	public QAFWebElement getPassword() {
		return password;
	}

	public QAFWebElement getSignInButton() {
		return signInButton;
	}
	
	public QAFWebElement getErrorMessage() {
		return errorMessage;
	}
	
	//Verify Information on CEO Sign In Page
	public void verifyInformation()
	{
		CEOSignInPage signInPage = new CEOSignInPage();
		
		for(int information=0 ; information<signInPage.getInformationComponent().size()-1;information++)
		{
			signInPage.getInformationComponent().get(information).getHeaderText().verifyText(ConfigurationManager.getBundle().getString("header"+information+".text"), "");
			signInPage.getInformationComponent().get(information).getContentText().verifyText(ConfigurationManager.getBundle().getString("content"+information+".text"), "");
		}
	}
	
	/*Verify Error Message when Invalid credential are provided */
	public void verifyErrorMessage()
	{
		CEOSignInPage signInPage = new CEOSignInPage();
		signInPage.getErrorMessage().verifyText(ConfigurationManager.getBundle().getString("error.text"),
				ConfigurationManager.getBundle().getString("error_label.text"));
	
	}
	
	
	
	/* Fill Data from examples */
	public void fillSignInData(String str0, String str1, String str2)
	{
		CEOSignInPage signIn = new CEOSignInPage();
		signIn.getCompanyId().sendKeys(str0);
		signIn.getUserId().sendKeys(str1);
		signIn.getPassword().sendKeys(str2);
		signIn.getSignInButton().click();
	}
	
	
	@Override
	protected void openPage(PageLocator arg0, Object... arg1) {
		// TODO Auto-generated method stub

	}

}