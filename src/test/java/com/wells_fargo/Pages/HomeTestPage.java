package com.wells_fargo.Pages;

import java.util.List;

import org.hamcrest.Matchers;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.qmetry.qaf.automation.core.QAFTestBase;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.selenium.AssertionService;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Validator;
import com.wells_fargo.Component.HomePageComponent;
import com.wells_fargo.Util.WellsForgoProperties;

public class HomeTestPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "home.page.menu.component.loc")
	private HomePageComponent menuComponent;
	@FindBy(locator= "commercial.link")
	private QAFWebElement commercialLink;
	@FindBy(locator="btn.BorrowingandCredit.homepage")
	private QAFWebElement btnBorrowingandCredit;

	@Override
	protected void openPage(PageLocator pageLocator, Object... args) {
		driver.get("/");
	}
	

	public void waitForPageToLoad() {
		try {
			getMenuComponent().waitForPresent();
			getMenuComponent().waitForVisible();
		} catch (Exception e) {
			AssertionService
					.fail("Not able to find " + WellsForgoProperties.WELLS_FORGO_HOME_PAGE + " page");
		}
	}
	
	
	public HomePageComponent getMenuComponent() {
		return menuComponent;

	
	}

	// Verifies Wells Forgo home page
	public void verifyWellsForgoHomePage() {

		Validator.verifyThat("Wells Forgo home page", driver.getCurrentUrl().equalsIgnoreCase("https://www.wellsfargo.com/"), Matchers.equalTo(true));
	}
	
	
	public QAFWebElement getCommercialLink() {
		return commercialLink;
	}
	
	public void clickBorrowingandCredit()
	{
		
		List<WebElement> Bnc = driver.findElements(By.xpath(".//*[@id=\"mainContent\"]/div[2]/div[1]/ul/li"));
		for (int i = 0; i < Bnc.size(); i++) 
		{
			String name = driver.findElement(By.xpath("/html/body/div/div[2]/div[2]/div[1]/ul/li[2]/div")).getText();
			QAFTestBase.pause(4000);
			if(name.equalsIgnoreCase("Borrowing and Credit"))
			{
				QAFTestBase.pause(3000);
				btnBorrowingandCredit.click();
				break;
			}
			else {
				driver.findElement(By.xpath("//*[@id=\"mainContent\"]/div[2]/a[2]/img")).click();
			}
		}
	}
}

