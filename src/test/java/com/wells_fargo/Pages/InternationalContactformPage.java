package com.wells_fargo.Pages;

import org.openqa.selenium.Keys;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.wells_fargo.Bean.InternationalServicesContactUsBean;
import com.wells_fargo.Util.WellsForgoUtility;

public class InternationalContactformPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "select.international.services.names.loc")
	private QAFWebElement nameTextBox;
	@FindBy(locator = "txt.international.services.first.name.loc")
	private QAFWebElement firstNameTextBox;
	@FindBy(locator = "txt.international.services.surname.name.loc")
	private QAFWebElement surnameTextBox;
	@FindBy(locator = "txt.international.services.corporate.title.loc")
	private QAFWebElement corporateTitleTextBox;
	@FindBy(locator = "txt.international.services.company.name.loc")
	private QAFWebElement companyNameTextBox;
	@FindBy(locator = "txt.international.services.company.website.loc")
	private QAFWebElement companyWebsiteTextBox;
	@FindBy(locator = "select.international.services.annual.revenue.loc")
	private QAFWebElement annualRevenueTextBox;
	@FindBy(locator = "select.international.services.is.wells.customer.loc")
	private QAFWebElement isWellsCustomerTextBox;
	@FindBy(locator = "txt.international.services.first.address.loc")
	private QAFWebElement firstAddressTextBox;
	@FindBy(locator = "txt.international.services.second.address.loc")
	private QAFWebElement secondAddressTextBox;
	@FindBy(locator = "txt.international.services.city.loc")
	private QAFWebElement cityTextBox;
	@FindBy(locator = "txt.international.services.state.loc")
	private QAFWebElement stateTextBox;
	@FindBy(locator = "txt.international.services.zip.loc")
	private QAFWebElement zipTextBox;
	@FindBy(locator = "txt.international.services.country.loc")
	private QAFWebElement countryTextBox;
	@FindBy(locator = "txt.international.services.usphone.number.loc")
	private QAFWebElement usPhoneNumberTextBox;
	@FindBy(locator = "txt.international.services.international.phone.number.loc")
	private QAFWebElement internationalPhoneNumberTextBox;
	@FindBy(locator = "txt.international.services.email.address.loc")
	private QAFWebElement emailTextBox;
	@FindBy(locator = "select.international.services.information.needed.loc")
	private QAFWebElement inforamtionNeededTextBox;
	@FindBy(locator = "txt.international.services.help.loc")
	private QAFWebElement helpTextBox;

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub

	}

		public QAFWebElement getNameTextBox() {
			return nameTextBox;
		}
		
	public QAFWebElement getFirstNameTextBox() {
		return firstNameTextBox;
	}

	public QAFWebElement getSurnameTextBox() {
		return surnameTextBox;
	}

	public QAFWebElement getCorporateTitleTextBox() {
		return corporateTitleTextBox;
	}

	public QAFWebElement getCompanyNameTextBox() {
		return companyNameTextBox;
	}

	public QAFWebElement getCompanyWebsiteTextBox() {
		return companyWebsiteTextBox;
	}

	public QAFWebElement getAnnualRevenueTextBox() {
		return annualRevenueTextBox;
	}

	public QAFWebElement getIsWellsCustomerTextBox() {
		return isWellsCustomerTextBox;
	}

	public QAFWebElement getFirstAddressTextBox() {
		return firstAddressTextBox;
	}

	public QAFWebElement getSecondAddressTextBox() {
		return secondAddressTextBox;
	}

	public QAFWebElement getCityTextBox() {
		return cityTextBox;
	}

	public QAFWebElement getStateTextBox() {
		return stateTextBox;
	}
	
		public QAFWebElement getZipTextBox() {
			return zipTextBox;
		}
		
		public QAFWebElement getCountryTextBox() {
			return countryTextBox;
		}
		
		public QAFWebElement getUsPhoneNumberTextBox() {
			return usPhoneNumberTextBox;
		}
		
		public QAFWebElement getInternationalPhoneNumberTextBox() {
			return internationalPhoneNumberTextBox;
		}
		
		public QAFWebElement getEmailTextBox() {
			return emailTextBox;
		}
		
		public QAFWebElement getInforamtionNeededTextBox() {
			return inforamtionNeededTextBox;
		}
		
		public QAFWebElement getHelpTextBox() {
			return helpTextBox;
		}

		// Fill contact form data
		public void fillData()
		{
			InternationalServicesContactUsBean contactUsBean = new InternationalServicesContactUsBean();
			WellsForgoUtility wellsForgoUtility = new WellsForgoUtility();
			contactUsBean.fillRandomData();
			getCityTextBox().verifyVisible();
			wellsForgoUtility.scrollToCenter(companyNameTextBox);
			getNameTextBox().sendKeys(contactUsBean.getName(),Keys.ARROW_DOWN,Keys.ENTER);
			getFirstNameTextBox().sendKeys(contactUsBean.getFirstName());
			getSurnameTextBox().sendKeys(contactUsBean.getSurname());
			getCompanyNameTextBox().sendKeys(contactUsBean.getCompanyName());
			getCompanyWebsiteTextBox().sendKeys(contactUsBean.getCompanyWebsite());
			getCorporateTitleTextBox().sendKeys(contactUsBean.getCorporateTitle());
			getCityTextBox().sendKeys(contactUsBean.getCity());
			getCountryTextBox().sendKeys(contactUsBean.getCountry());
			getStateTextBox().sendKeys(contactUsBean.getState());
			getZipTextBox().sendKeys(contactUsBean.getZip());
			getFirstAddressTextBox().sendKeys(contactUsBean.getFirstAddress());
			getSecondAddressTextBox().sendKeys(contactUsBean.getSecondAddress());
			getHelpTextBox().sendKeys(contactUsBean.getHelp());
			getIsWellsCustomerTextBox().sendKeys(contactUsBean.getIsWellsCustomer(),Keys.ARROW_DOWN,Keys.ENTER);
			getInforamtionNeededTextBox().sendKeys(contactUsBean.getInforamtionNeeded(),Keys.ARROW_DOWN,Keys.ENTER);
			getInternationalPhoneNumberTextBox().sendKeys(contactUsBean.getInternationalPhoneNumber());
			getUsPhoneNumberTextBox().sendKeys(contactUsBean.getUsPhoneNumber());
			getEmailTextBox().sendKeys(contactUsBean.getEmail());
			getAnnualRevenueTextBox().sendKeys(contactUsBean.getAnnualRevenue(),Keys.ARROW_DOWN,Keys.ENTER);
			
			Reporter.log("Customer fills all the fields successfully");
		}
}