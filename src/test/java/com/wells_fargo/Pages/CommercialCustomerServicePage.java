package com.wells_fargo.Pages;

import org.hamcrest.Matchers;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.selenium.AssertionService;
import com.qmetry.qaf.automation.util.Validator;
import com.wells_fargo.Component.CustomerServiceComponent;
import com.wells_fargo.Util.WellsForgoProperties;

public class CommercialCustomerServicePage extends WebDriverBaseTestPage<WebDriverTestPage>{
	@FindBy(locator = "commercial.customer.service.component.loc")
	private CustomerServiceComponent customerServiceComponent;

	protected void openPage(PageLocator locator, Object... args) {

	}
	
	public void waitForPageToLoad() {
		try {
			getCustomerServiceComponent().waitForPresent();
			getCustomerServiceComponent().waitForVisible();
		} catch (Exception e) {
			AssertionService
					.fail("Not able to find " + WellsForgoProperties.COMMERCIAL_CUSTOMER_SERVICE_PAGE + " page");
		}
	}

	public CustomerServiceComponent getCustomerServiceComponent() {
		return customerServiceComponent;
	}
	
	// Verifies customer service page

		public void verifyCustomerServicePage() {

			Validator.verifyThat("Customer service page", driver.getCurrentUrl().contains("customer-service"), Matchers.equalTo(true));
		}
}
