package com.wells_fargo.Pages;

import org.openqa.selenium.Keys;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.wells_fargo.Bean.CommercialContactUsBean;
import com.wells_fargo.Util.WellsForgoUtility;

public class CommercialContactUsPage extends WebDriverBaseTestPage<WebDriverTestPage> {

	@FindBy(locator = "select.commercial.title.loc")
	private QAFWebElement titleTextBox;
	@FindBy(locator = "txt.commercial.first.name.loc")
	private QAFWebElement firstNameTextBox;
	@FindBy(locator = "txt.commercial.surname.name.loc")
	private QAFWebElement surnameTextBox;
	@FindBy(locator = "txt.commercial.corporate.title.loc")
	private QAFWebElement corporateTitleTextBox;
	@FindBy(locator = "txt.commercial.company.name.loc")
	private QAFWebElement companyNameTextBox;
	@FindBy(locator = "txt.commercial.company.website.loc")
	private QAFWebElement companyWebsiteTextBox;
	@FindBy(locator = "select.commercial.annual.revenue.loc")
	private QAFWebElement annualRevenueTextBox;
	@FindBy(locator = "select.commercial.is.wells.customer.loc")
	private QAFWebElement isWellsCustomerTextBox;
	@FindBy(locator = "txt.commercial.first.address.loc")
	private QAFWebElement firstAddressTextBox;
	@FindBy(locator = "txt.commercial.second.address.loc")
	private QAFWebElement secondAddressTextBox;
	@FindBy(locator = "txt.commercial.city.loc")
	private QAFWebElement cityTextBox;
	@FindBy(locator = "txt.commercial.state.loc")
	private QAFWebElement stateTextBox;
	@FindBy(locator = "txt.commercial.zip.loc")
	private QAFWebElement zipTextBox;
	@FindBy(locator = "txt.commercial.country.loc")
	private QAFWebElement countryTextBox;
	@FindBy(locator = "txt.commercial.usphone.number.loc")
	private QAFWebElement usPhoneNumberTextBox;
	@FindBy(locator = "txt.commercial.international.phone.number.loc")
	private QAFWebElement internationalPhoneNumberTextBox;
	@FindBy(locator = "txt.commercial.email.address.loc")
	private QAFWebElement emailTextBox;
	@FindBy(locator = "select.commercial.information.needed.loc")
	private QAFWebElement inforamtionNeededTextBox;
	@FindBy(locator = "txt.commercial.help.loc")
	private QAFWebElement helpTextBox;
	@FindBy(locator = "link.commercial.page.contact.us.loc")
	private QAFWebElement contactUsLink;

	@Override
	protected void openPage(PageLocator locator, Object... args) {
		// TODO Auto-generated method stub

	}

		public QAFWebElement getTitleTextBox() {
			return titleTextBox;
		}
		
	public QAFWebElement getFirstNameTextBox() {
		return firstNameTextBox;
	}

	public QAFWebElement getSurnameTextBox() {
		return surnameTextBox;
	}

	public QAFWebElement getCorporateTitleTextBox() {
		return corporateTitleTextBox;
	}

	public QAFWebElement getCompanyNameTextBox() {
		return companyNameTextBox;
	}

	public QAFWebElement getCompanyWebsiteTextBox() {
		return companyWebsiteTextBox;
	}

	public QAFWebElement getAnnualRevenueTextBox() {
		return annualRevenueTextBox;
	}

	public QAFWebElement getIsWellsCustomerTextBox() {
		return isWellsCustomerTextBox;
	}

	public QAFWebElement getFirstAddressTextBox() {
		return firstAddressTextBox;
	}

	public QAFWebElement getSecondAddressTextBox() {
		return secondAddressTextBox;
	}

	public QAFWebElement getCityTextBox() {
		return cityTextBox;
	}

	public QAFWebElement getStateTextBox() {
		return stateTextBox;
	}
	
		public QAFWebElement getZipTextBox() {
			return zipTextBox;
		}
		
		public QAFWebElement getCountryTextBox() {
			return countryTextBox;
		}
		
		public QAFWebElement getUsPhoneNumberTextBox() {
			return usPhoneNumberTextBox;
		}
		
		public QAFWebElement getInternationalPhoneNumberTextBox() {
			return internationalPhoneNumberTextBox;
		}
		
		public QAFWebElement getEmailTextBox() {
			return emailTextBox;
		}
		
		public QAFWebElement getInforamtionNeededTextBox() {
			return inforamtionNeededTextBox;
		}
		
		public QAFWebElement getHelpTextBox() {
			return helpTextBox;
		}
		
		public QAFWebElement getContactUsLink() {
			return contactUsLink;
		}

		// Fill contact form data for XML
		public void fillData()
		{
			CommercialContactUsBean commercialContactUsBean = new CommercialContactUsBean();
			WellsForgoUtility wellsForgoUtility = new WellsForgoUtility();
			commercialContactUsBean.fillFromConfig("user");
			getCityTextBox().verifyVisible();
			wellsForgoUtility.scrollToCenter(companyNameTextBox);
			getTitleTextBox().sendKeys(commercialContactUsBean.getTitle(),Keys.ARROW_DOWN,Keys.ENTER);
			getFirstNameTextBox().sendKeys(commercialContactUsBean.getFirstName());
			getSurnameTextBox().sendKeys(commercialContactUsBean.getSurname());
			getCompanyNameTextBox().sendKeys(commercialContactUsBean.getCompanyName());
			getCompanyWebsiteTextBox().sendKeys(commercialContactUsBean.getCompanyWebsite());
			getCityTextBox().sendKeys(commercialContactUsBean.getCity());
			getCountryTextBox().sendKeys(commercialContactUsBean.getCountry());
			getStateTextBox().sendKeys(commercialContactUsBean.getState());
			getZipTextBox().sendKeys(commercialContactUsBean.getZip());
			getFirstAddressTextBox().sendKeys(commercialContactUsBean.getFirstAddress());
			getSecondAddressTextBox().sendKeys(commercialContactUsBean.getSecondAddress());
			getHelpTextBox().sendKeys(commercialContactUsBean.getHelp());
			getIsWellsCustomerTextBox().sendKeys(commercialContactUsBean.getIsWellsCustomer(),Keys.ARROW_DOWN,Keys.ENTER);
			getInforamtionNeededTextBox().sendKeys(commercialContactUsBean.getInforamtionNeeded(),Keys.ARROW_DOWN,Keys.ENTER);
			getInternationalPhoneNumberTextBox().sendKeys(commercialContactUsBean.getInternationalPhoneNumber());
			getUsPhoneNumberTextBox().sendKeys(commercialContactUsBean.getUsPhoneNumber());
			getEmailTextBox().sendKeys(commercialContactUsBean.getEmail());
			getAnnualRevenueTextBox().sendKeys(commercialContactUsBean.getAnnualRevenue(),Keys.ARROW_DOWN,Keys.ENTER);
			
			Reporter.log("Customer fills all the fields successfully");
		}
		
}