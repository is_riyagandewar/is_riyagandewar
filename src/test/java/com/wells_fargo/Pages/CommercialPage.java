package com.wells_fargo.Pages;


import java.util.List;

import com.qmetry.qaf.automation.core.MessageTypes;
import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.selenium.AssertionService;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.qmetry.qaf.automation.util.Reporter;
import com.wells_fargo.Util.WellsForgoProperties;
import com.wells_fargo.Util.WellsForgoUtility;

public class CommercialPage extends WebDriverBaseTestPage<WebDriverTestPage> {
	
	@FindBy(locator = "link.commercial.page.customer.service.loc")
	private QAFWebElement customerServiceLink;
	
	
	
	protected void openPage(PageLocator locator, Object... args) {

	}
	
	public void waitForPageToLoad() {
		try {
			getCustomerServiceLink().waitForPresent();
			getCustomerServiceLink().waitForVisible();
		} catch (Exception e) {
			AssertionService
					.fail("Not able to find " + WellsForgoProperties.COMMERCIAL_PAGE + " page");
		}
	}

	public QAFWebElement getCustomerServiceLink() {
		return customerServiceLink;
	}
    // Clicks on Element in all pages
	public void clickOnElement(String elementName) {
		CommercialCustomerServicePage commercialCustomerServicePage = new CommercialCustomerServicePage();
		CommercialContactUsPage commercialContactUsPage = new CommercialContactUsPage();
		RoutingNumbersPage routingNumbersPage = new RoutingNumbersPage();
		WellsForgoUtility WellsForgoUtility = new WellsForgoUtility();
		
			if (elementName.equals(WellsForgoProperties.CUSTOMER_SERVICE)) {
				getCustomerServiceLink().click();
				Reporter.log(WellsForgoProperties.CUSTOMER_SERVICE + " " + WellsForgoProperties.LINK
						+ " clicked successfully", MessageTypes.Pass);
			} else if (elementName.equalsIgnoreCase(WellsForgoProperties.INTERNATIONAL_ROUTING_NUMBERS)) {
				List<QAFWebElement> routingNumbersList = commercialCustomerServicePage.getCustomerServiceComponent().getRoutingNumbersList();
				for(QAFWebElement routingNumber:routingNumbersList ){
					if(routingNumber.getText().trim().equalsIgnoreCase(elementName))
					{
						routingNumber.click();
						Reporter.log(WellsForgoProperties.INTERNATIONAL_ROUTING_NUMBERS + " " + WellsForgoProperties.LINK
								+ " clicked successfully", MessageTypes.Pass);
						break;
					}
				}
			} 			
			else if (elementName.equals(WellsForgoProperties.CONTACT_US_ONLINE)) {
				WellsForgoUtility.scrollToCenter(commercialContactUsPage.getContactUsLink());
					commercialContactUsPage.getContactUsLink().click();
					Reporter.log(WellsForgoProperties.CONTACT_US_ONLINE + " " + WellsForgoProperties.LINK
							+ " clicked successfully", MessageTypes.Pass);
				}
			else if (elementName.equals(WellsForgoProperties.CONTACT_FORM)) {
					routingNumbersPage.getcontactFormLink().click();
					Reporter.log(WellsForgoProperties.CONTACT_FORM + " " + WellsForgoProperties.LINK
							+ " clicked successfully", MessageTypes.Pass);
				}
	}
}
