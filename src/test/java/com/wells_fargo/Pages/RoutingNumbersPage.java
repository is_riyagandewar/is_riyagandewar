package com.wells_fargo.Pages;

import com.qmetry.qaf.automation.ui.WebDriverBaseTestPage;
import com.qmetry.qaf.automation.ui.annotations.FindBy;
import com.qmetry.qaf.automation.ui.api.PageLocator;
import com.qmetry.qaf.automation.ui.api.WebDriverTestPage;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebElement;
import com.wells_fargo.Component.RoutingNumbersComponent;

public class RoutingNumbersPage extends WebDriverBaseTestPage<WebDriverTestPage>{
	
	@FindBy(locator = "routing.numbers.component.table.loc")
	private RoutingNumbersComponent usRoutingNumbersTable;
	@FindBy(locator = "link.routing.numbers.contact.form.loc")
	private QAFWebElement contactFormLink;
	
	protected void openPage(PageLocator pageLocator, Object... args) {
		
	}
	
	public RoutingNumbersComponent getUsRoutingNumbersTable() {
		return usRoutingNumbersTable;
	}public QAFWebElement getcontactFormLink() {
		return contactFormLink;
	}
}
