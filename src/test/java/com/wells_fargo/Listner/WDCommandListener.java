package com.wells_fargo.Listner;

import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebDriver;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebDriverCommandAdapter;

public class WDCommandListener extends QAFWebDriverCommandAdapter {

	@Override
	public void onInitialize(QAFExtendedWebDriver driver) {

		super.onInitialize(driver);
		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
	}
}
 