package com.wells_fargo.Listner;

import com.qmetry.qaf.automation.ui.webdriver.QAFExtendedWebDriver;
import com.qmetry.qaf.automation.ui.webdriver.QAFWebDriverCommandAdapter;
/*
 * Listner to Maximize the browser window at initialization
*/
public class DriverListener extends QAFWebDriverCommandAdapter {

	public void onInitialize(QAFExtendedWebDriver driver) {
		driver.manage().window().maximize();
	}
}
