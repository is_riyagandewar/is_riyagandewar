@FunctionalTest
Feature: Functionality to verify information on signIn page.

Background:  User navigates to commercial page
	
	Given user navigates to wellsfargo home page
	Then user verify wellsfargo home page is opened
	When navigates to 'Commercial' page 
	
@FunctionalTest
Scenario Outline: Verify expected error message when singnin with incorect credencial

	Given navigate to ceo sign in page
	When fill incorrect "<companyid>", "<userid>", "<password>" signin data
	Then verify error message

Examples: 
      | companyid | userid  | password   	| 
      | A1234     | test1   | testing123 	| 
      | B1234     | test2   | testing123# | 
      
@SmokeTest
Scenario: Verify information on sign in page

	Given navigate to ceo sign in page
	Then verify information on sign in page