@SmokeTest
Feature: Functionality to verify information on commercialPage

Background:  User navigates to commercial page
	
	Given user navigates to wellsfargo home page
	Then user verify wellsfargo home page is opened
	When navigates to 'Commercial' page 
	
@SmokeTest
Scenario Outline: User verifies routing numbers data of U.S. locations
	
	Given clicks on 'Customer Service' link in Commercial page
	Then user verify customer service page is opened
	When clicks on 'International Routing Numbers' link in Customer Service page
	And user verifies '${city}' and '${name}' of U.S. locations in Routing Numbers page

@RegressionTest
Scenario: User fill contact us form on Commercial page for XML

	Given clicks on 'Customer Service' link in Commercial page
	Then user verify customer service page is opened
	When clicks on 'Contact Us Online' link in Customer Service page
	And user fill contact us form on Commercial page

