@RegressionTest
Feature: Functionality to verify information on international servicePage

Background:  User navigates to commercial page
	
	Given user navigates to wellsfargo home page
	Then user verify wellsfargo home page is opened
	When navigates to 'Commercial' page 
	
@RegressionTest
Scenario: User fill contact form on international service page
	
	Given clicks on 'Customer Service' link in Commercial page
	Then user verify customer service page is opened
	When clicks on 'International Routing Numbers' link in Customer Service page
	And clicks on 'contact form' link in Routing Number page
	Then customer fill contact form on International Service page